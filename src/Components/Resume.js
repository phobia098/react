import React, { Component } from "react";
import Slide from "react-reveal";

class Resume extends Component {
  getRandomColor() {
    let letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  render() {
    if (!this.props.data) return null;

    const skillmessage = this.props.data.skillmessage;
    const education = this.props.data.education.map(function (education) {
      return (
        <div key={education.school} style={{color: 'white'}}>
          <h3 style={{color: 'white'}}>{education.school}</h3>
          <p className="info" style={{color: 'white'}}>
            {education.degree} <span>&bull;</span>
            <em className="date">{education.graduated}</em>
          </p>
          <p>{education.description}</p>
        </div>
      );
    });

    const work = this.props.data.work.map(function (work) {
      return (
        <div key={work.company}>
          <h3 style={{color: 'white'}}>{work.company}</h3>
          <p className="info" style={{color: 'white'}}>
            {work.title}
            <span>&bull;</span> <em className="date">{work.years}</em>
          </p>
          <p>{work.description}</p>
        </div>
      );
    });

    const skills = this.props.data.skills.map((skills) => {
      const backgroundColor = this.getRandomColor();
      const className = "bar-expand " + skills.name.toLowerCase();
      const width = skills.level;

      return (
        <li key={skills.name} style={{width: '60%', height: '10px'}}>
          <span style={{ width, backgroundColor, height: '10px' }} className={className}></span>
          <em style={{color: 'white'}}>{skills.name}</em>
        </li>
      );
    });

    return (
      <section id="resume" style={{backgroundColor: 'black', color: 'white'}}>
        <Slide left duration={1300}>
          <div className="row education">
            <div className="three columns header-col">
              <h1>
                <span  style={{color: 'white'}}>Education</span>
              </h1>
            </div>

            <div className="nine columns main-col"  style={{color: 'white'}}>
              <div className="row item"  style={{color: 'white'}}>
                <div className="twelve columns" style={{color: 'white'}}>{education}</div>
              </div>
            </div>
          </div>
        </Slide>

        <Slide left duration={1300}>
          <div className="row work">
            <div className="three columns header-col">
              <h1>
                <span  style={{color: 'white'}}>Work</span>
              </h1>
            </div>

            <div className="nine columns main-col">{work}</div>
          </div>
        </Slide>

        <Slide left duration={1300}>
          <div className="row skill">
            <div className="three columns header-col">
              <h1>
                <span style={{color: 'white'}}>Skills</span>
              </h1>
            </div>

            <div className="nine columns main-col">
              <p  style={{color: 'white'}}>{skillmessage}</p>

              <div className="bars" >
                <ul className="skills" >{skills}</ul>
              </div>
            </div>
          </div>
        </Slide>
      </section>
    );
  }
}

export default Resume;
